package codenuggets.hackerrank;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AlmostSortedTest {
	AlmostSorted as = null;

	@Before
	public void init() {
		as = new AlmostSorted();

	}

	@Test
	public void testCalculatePermutation(){
		assertEquals(9 , as.countAlmostSorted(new long[]{3,1,2,6,5,4}));
		assertEquals(3 , as.countAlmostSorted(new long[]{1,2}));		
		assertEquals(2 , as.countAlmostSorted(new long[]{2,1}));
		assertEquals(3 , as.countAlmostSorted(new long[]{2,1,0}));
		assertEquals(8 , as.countAlmostSorted(new long[]{3, 1, 2, 5, 4}));
		
	}
}
