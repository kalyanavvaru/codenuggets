package codenuggets.hackerrank;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TemplateTest extends Template {
	Template template;
	
	@Before
	public void init(){
		template = new Template(); 
	}

	@Test
	public void testGenerateBase7() {
		char[] base7 = new char[]{'0','a','t','l','s','i','n'};
		assertEquals("a0",template.generateBase7(7));//10
		assertEquals("t0",template.generateBase7(14));//20
		assertEquals("l0",template.generateBase7(21));//30
		assertEquals("s0",template.generateBase7(28));//40
		assertEquals("i0",template.generateBase7(35));//50
		assertEquals("n0",template.generateBase7(42));//60
		
		assertEquals("a00",template.generateBase7(49));//70
		assertEquals("a0a",template.generateBase7(50));//71
		
		assertEquals("aa0",template.generateBase7(56));//80
		assertEquals("at0",template.generateBase7(63));//90
		assertEquals("al0",template.generateBase7(70));//100
		assertEquals("as0",template.generateBase7(77));//110
		assertEquals("ai0",template.generateBase7(84));//120		
		assertEquals("an0",template.generateBase7(91));//130
		
		assertEquals("t00",template.generateBase7(98));//140
		assertEquals("ta0",template.generateBase7(105));//150
		assertEquals("atlassian",template.generateBase7(7792875));//150
  }

}
