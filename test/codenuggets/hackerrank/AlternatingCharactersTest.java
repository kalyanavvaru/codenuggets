package codenuggets.hackerrank;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class AlternatingCharactersTest {
	AlternatingCharacters ac = null;
	
	
	@Before
	public void init(){
		ac = new AlternatingCharacters();
	}
	
	@Test
	public void testAlternatingCharacters() {
		assertEquals( 3 ,ac.calculateDeletions("AAAA"));
		assertEquals( 4 ,ac.calculateDeletions("BBBBB"));
		assertEquals( 0 ,ac.calculateDeletions("ABABABAB"));
		assertEquals( 0 ,ac.calculateDeletions("BABABA"));
		assertEquals( 4 ,ac.calculateDeletions("AAABBB"));		
	}

}
