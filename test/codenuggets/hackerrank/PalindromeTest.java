package codenuggets.hackerrank;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import codenuggets.hackerrank.PalindromeAnalyzer.ReflectionPair;


public class PalindromeTest {
	
	PalindromeAnalyzer palAnalyzer = null;
	
	@Before
	public void init(){
		palAnalyzer = new PalindromeAnalyzer();
	}
	
	@Test
	public void testPalindrome(){
		List<ReflectionPair> palList = new ArrayList<ReflectionPair>();
		assertNotNull(palAnalyzer.analyzePalindrome("aba"));
		assertEquals(0,palAnalyzer.analyzePalindrome("aba").size());
		
		
		palList.add(new ReflectionPair(1,2));
		assertEquals(palList,palAnalyzer.analyzePalindrome("abca"));
		
		palList.clear();
		palList.add(new ReflectionPair(1,2));
		assertEquals(palList,palAnalyzer.analyzePalindrome("abca"));
		
		palList.clear();
		palList.add(new ReflectionPair(1,2));
		palList.add(new ReflectionPair(0,3));
		
		Collections.sort(palList);
		List<ReflectionPair> result = palAnalyzer.analyzePalindrome("dbca");
		Collections.sort(result);
		assertEquals(palList,result);
		
		palList.clear();
		palList.add(new ReflectionPair(1,9));
		palList.add(new ReflectionPair(2,8));
		palList.add(new ReflectionPair(4,6));
		Collections.sort(palList);
		result = palAnalyzer.analyzePalindrome("abrakabadra");
		Collections.sort(result);
		assertEquals(palList,result);
	}
	
	@Test
	public void testPrintNumberofEditsRequired(){
		List<String> strings = new ArrayList<String>();
		strings.add("abc");
		strings.add("abcba");
		strings.add("abcd");
		strings.add("a");
		strings.add("aba");
		strings.add("abrakadabra");
		
		List<Integer> result = new ArrayList<Integer>();
		result.add(2);
		result.add(0);
		result.add(4);
		result.add(0);
		result.add(0);
		result.add(39);
		
		assertEquals(result,palAnalyzer.getNumberofEditsRequired(strings));
	}
	
	@Test
	public void testPrintNumberofEditsRequiredForString(){
		assertEquals(39,palAnalyzer.getNumberofEditsRequired("abrakadabra"));
	}
}
