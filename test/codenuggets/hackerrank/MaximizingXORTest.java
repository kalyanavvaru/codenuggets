package codenuggets.hackerrank;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;

import org.junit.Before;
import org.junit.Test;

public class MaximizingXORTest {
	MaximizingXOR xor = null;
	
	@Before
	public void init(){
		xor = new MaximizingXOR();
	}
	
	@Test
	public void testInputFromKeyBoard() {
		String data="10\n15\n";		
		System.setIn(new ByteArrayInputStream(data.getBytes()));
     	assertArrayEquals(new int[]{10,15},xor.testInputFromKeyBoard());
     	data="0\n1\n";		
		System.setIn(new ByteArrayInputStream(data.getBytes()));
		assertArrayEquals(new int[]{0,1},xor.testInputFromKeyBoard());
	}

	
	@Test
	public void testMaxXor(){
		assertEquals(7,xor.maxXor(10,15));
		assertEquals(3,xor.maxXor(0,2));
		assertEquals(1,xor.maxXor(0,1));
		
	}
}
