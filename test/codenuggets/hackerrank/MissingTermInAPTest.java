package codenuggets.hackerrank;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class MissingTermInAPTest {
	
	MissingTermAp ap = null;
	
	@Before
	public void init(){
		ap = new MissingTermAp();
	}
	
	@Test
	public void testGetFrequency(){
		assertEquals(1,ap.getFrequency(new int[]{1,2,3}));
		assertEquals(2,ap.getFrequency(new int[]{1,3,5}));
		assertEquals(2,ap.getFrequency(new int[]{1,3,5,7}));
		assertEquals(2,ap.getFrequency(new int[]{1,3,5,9}));
	}

	@Test
	public void testGetMissingFromAP() {
		assertEquals(7,ap.getMissingFromAP(new int[]{1,3,5,9}));
		assertEquals(15,ap.getMissingFromAP(new int[]{0,5,10,20}));
		assertEquals(0,ap.getMissingFromAP(new int[]{1,2,3,4}));
		
	}
}
