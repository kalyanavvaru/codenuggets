package codenuggets.hackerrank;
import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;

import org.junit.Before;
import org.junit.Test;


public class UtopianTreeSolutionTest {
	UtopianTreeSolution runner = null;
	
	@Before
	public void init(){
		runner = new UtopianTreeSolution();
	}

	@Test
	public void testUtopianTree() {
		int[] input = {1,2};
		feedToSystemIn(input);    	
		assertArrayEquals(new Integer[]{3},runner.doJob());
		
		input = new int[]{2,3,4};
		feedToSystemIn(input);    	
		assertArrayEquals(new Integer[]{6,7},runner.doJob());
		
		input = new int[]{2,0,1};
		feedToSystemIn(input);    	
		assertArrayEquals(new Integer[]{1,2},runner.doJob());
	}
	
	@Test
	public void testLengthAfterCycles() {
		assertEquals(1,runner.lengthAfterCycles(0));
		assertEquals(2,runner.lengthAfterCycles(1));
		assertEquals(3,runner.lengthAfterCycles(2));
		assertEquals(6,runner.lengthAfterCycles(3));
		assertEquals(7,runner.lengthAfterCycles(4));
		assertEquals(14,runner.lengthAfterCycles(5));
		assertEquals(15,runner.lengthAfterCycles(6));
	}
	
	@Test
	public void testDouble() {
		assertEquals(0,runner.doDouble(0));
		assertEquals(2,runner.doDouble(1));
		assertEquals(120,runner.doDouble(60));
	}

	@Test
	public void testPlusOne() {
		assertEquals(0,runner.doPlusOne(0));
		assertEquals(2,runner.doPlusOne(1));
		assertEquals(61,runner.doPlusOne(60));
	}
	
	@Test
	public void testHalfCycle() {
		assertEquals(0,runner.doHalfCycle(0));
		assertEquals(2,runner.doHalfCycle(1));
		assertEquals(120,runner.doHalfCycle(60));
	}
	
	@Test
	public void testFullCycle() {
		assertEquals(3,runner.doFullCycle(1));
		assertEquals(7,runner.doFullCycle(3));
		assertEquals(15,runner.doFullCycle(7));
		
		assertEquals(0,runner.doFullCycle(0));
	}
	
	private void feedToSystemIn(int[] data) {
		String dataStr = "";
		for(int a:data){
			dataStr+=a+"\n";
		}
		System.setIn(new ByteArrayInputStream(dataStr.getBytes()));
	}

}
