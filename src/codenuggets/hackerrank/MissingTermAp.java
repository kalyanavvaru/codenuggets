package codenuggets.hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;

public class MissingTermAp {

	public static void main(String[] args) throws IOException {
		MissingTermAp ap = new MissingTermAp();
		ap.getFrequency(new int[] { 1, 2, 3 });
		int[] input = ap.readArrayFromKeyBoard();
		System.out.println(ap.getMissingFromAP(input));
	}

	private int[] readArrayFromKeyBoard() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int length = Integer.parseInt(br.readLine());
		int [] input = new int[length];
		String line = br.readLine();
		String[]  array = line.split(" ");
		for (int i = 0; i < length; i++) {
			input[i]=Integer.parseInt(array[i]);
		}
		return input;
	}

	public int getFrequency(int[] is) {
		Map<Integer, Integer> freqMap = new TreeMap<Integer, Integer>();

		for (int i = 1; i < is.length; i++) {
			int freq = is[i] - is[i - 1];
			int times = freqMap.containsKey(freq) ? freqMap.get(freq) : 0;
			freqMap.put(freq, times + 1);
		}

		int freq = 0;
		int maxOccur = 0;
		for (Map.Entry<Integer, Integer> entry : freqMap.entrySet()) {
			if (entry.getKey() == 0) {
				freq = entry.getKey();
				maxOccur = entry.getValue();
			}

			if (entry.getValue() > maxOccur) {
				freq = entry.getKey();
				maxOccur = entry.getValue();
			}

		}
		return freq;
	}

	public int getMissingFromAP(int[] is) {
		int returnVal = 0;
		int frequency = getFrequency(is);
		for (int i = 1; i < is.length; i++) {
			if(is[i] != is[i-1]+frequency){
				returnVal = is[i-1]+frequency;
			}
		}
		return returnVal;
	}

}
