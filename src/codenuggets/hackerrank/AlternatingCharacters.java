package codenuggets.hackerrank;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AlternatingCharacters {
	
	public static void main(String[] args) {
		AlternatingCharacters ac = new AlternatingCharacters();
		ac.doJob();
	}

	public void doJob() {
		Scanner in = new Scanner(System.in,"UTF-8");
		int numberOfStrings = Integer.parseInt(in.next());
		List<String> strings = new ArrayList<String>();
		for (int i = 0; i < numberOfStrings; i++) {
			strings.add(in.next());			
		}
		for(String string : strings){
			System.out.println(calculateDeletions(string));
		}

	}

	public int calculateDeletions(String string) {
		int deletions = 0;
		char prev = 0;
		for (int i = 0; i < string.length(); i++) {
			if (prev == 0) {
				prev = string.charAt(i);
				continue;
			}
			if (prev == string.charAt(i)) {
				deletions++;
			}

			prev = string.charAt(i);
		}

		return deletions;
	}
}
