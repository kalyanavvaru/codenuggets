package codenuggets.hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MaxPrime {
	public static void main(String[] args) throws IOException {
		MaxPrime mprime = new MaxPrime();
		int input = mprime.readIntFromKeyBoard();
		
			
		int totalprimes = 0;
		
		for (int j = 2; j < input; j++) {
			if((j!= 2 &&  j%2==0) || (j!=3 && j%3==0) || (j!=5 && j%5==0) || (j!=7 && j%7==0)) 
				continue;
		
			boolean notPrime = false;
			for(int i=2;i<j;i++){
				if(j%i == 0){
					notPrime = true;
				}
			}	
			if(!notPrime){
//				System.out.println(j);
				totalprimes++;	
			}
		}
		
		System.out.println(totalprimes);
	}
	private int readIntFromKeyBoard() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int length = Integer.parseInt(br.readLine());		
		return length;
	}
}
