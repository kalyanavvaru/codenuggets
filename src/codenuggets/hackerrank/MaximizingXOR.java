package codenuggets.hackerrank;

import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;

public class MaximizingXOR {
	
	public static void main(String[] args) {
		MaximizingXOR mXor = new MaximizingXOR();
		int [] input = mXor.testInputFromKeyBoard();
		System.out.println(mXor.maxXor(input[0],input[1]));
	}

	public int[] testInputFromKeyBoard() {
		int [] input = new int[2];
		Scanner in = new Scanner(System.in,"UTF-8");
		int l;
		l = Integer.parseInt(in.nextLine());

		int r;
		r = Integer.parseInt(in.nextLine());

		input[0] = l;
		input[1] = r;
		return input;
	}

	public int maxXor(int l, int r) {
		SortedSet<Integer> xors = new TreeSet<Integer>();
		for(int i = l ; i <= r ; i++){
			for(int j = l ; j <= r ; j++){
				if(i != j){
					xors.add(i ^ j);	
				}
			}	
		}
		return xors.last();
	}

	

}
