package codenuggets.hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class HalloweenParty {

	public static void main(String[] args) throws NumberFormatException, IOException {
		HalloweenParty party = new HalloweenParty();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		long cases = Long.parseLong(br.readLine());
		long[] testCases = new long[(int) cases]; 
		for (int i = 0; i < cases; i++) {
			testCases[i] = Long.parseLong(br.readLine());
		}
		for (int i = 0; i < testCases.length; i++) {
			System.out.println(party.getNumberOfSquares(testCases[i]));
		}
	}
	
	public long [] breakUpLines(long lines) {
		long[] breaks = new long[2];
		breaks[0] = lines/2;
		breaks[1] = Math.abs(lines - breaks[0]);
		Arrays.sort(breaks);
		return breaks;
	}

	public long getNumberOfSquares(long i) {
		long[] breaks = breakUpLines(i);
		return breaks[0] * breaks[1];
	}

}
