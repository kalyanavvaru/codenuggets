package codenuggets.hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class GemStones {
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		GemStones gs = new GemStones();
		Map<Character,Integer> map = gs.setValues();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		long cases = Long.parseLong(br.readLine());
		String[] testCases = new String[(int)cases]; 
		for (int i = 0; i < cases; i++) {
			testCases[i] = br.readLine();
		}
		
		for(String gemElement : testCases){
			gs.getGemStoneFrequency(gemElement,map);
		}
		
//		System.out.println(map);
		
		int totalGemElements =0 ;
		for(Map.Entry<Character, Integer> entry : map.entrySet()){
			if(entry.getValue() == cases){
				totalGemElements++;
			}
		}
		
		System.out.println(totalGemElements);
		
	}

	public Map<Character,Integer> getGemStoneFrequency(String string,Map<Character,Integer> map) {
		List<Character> list = new ArrayList<Character>();
		for(Character c : string.toCharArray()){
			
			Integer  value = map.get(c);
			if(value != null){
				if(!list.contains(c)){
					map.put(c, value+1);
					list.add(c);
				}
			}else{
				list.add(c);
				map.put(c, 1);
			}
		}
		return map;
	}
	
	public Map<Character,Integer> setValues(){
		Map<Character,Integer> map = new TreeMap<Character,Integer>();
		String allChars = "abcdefghijklmnopqrstuvwxyz";
		for(Character c : allChars.toCharArray()){
			map.put(c, 0);
		}
		return map;
	}

}
