package codenuggets.hackerrank;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class HalloweenPartyTest {

	HalloweenParty party = null;
	@Before
	public void init() {
		party = new HalloweenParty();
	}

	@Test
	public void testBreakUpLines(){
		assertArrayEquals(new long[]{2,2},party.breakUpLines(4));
		assertArrayEquals(new long[]{3,3},party.breakUpLines(6));
		assertArrayEquals(new long[]{3,4},party.breakUpLines(7));
		assertArrayEquals(new long[]{3,4},party.breakUpLines(7));
		assertArrayEquals(new long[]{7,8},party.breakUpLines(15));
		assertArrayEquals(new long[]{10,10},party.breakUpLines(20));
		assertArrayEquals(new long[]{0,0},party.breakUpLines(0));
		
	}

	
	@Test
	public void testNumberOfSquares(){
		assertEquals(4,party.getNumberOfSquares(4));
		assertEquals(9,party.getNumberOfSquares(6));
		assertEquals(12,party.getNumberOfSquares(7));
		assertEquals(56,party.getNumberOfSquares(15));
		assertEquals(0,party.getNumberOfSquares(0));
		assertEquals(1427060797812L,party.getNumberOfSquares(2389193));
	}

}
