package codenuggets.hackerrank;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import javax.jws.Oneway;

import org.junit.Before;
import org.junit.Test;

public class GemStonesTest {

	GemStones stones =  null;
	
	@Before
	public void init(){
		stones =new GemStones();
	}
	
	@Test
	public void testGetGemStoneFrequency() {
		Map<Character,Integer> map = new HashMap<Character,Integer>(){
			@Override
			public boolean equals(Object o) {
				if(o instanceof Map){
					Map<Character,Integer> another = ((Map<Character,Integer>)o);
					if(another.size()!=this.size()){
						return false;
					}
					for(Map.Entry<Character, Integer> entry : another.entrySet()){
						Character key = entry.getKey();
						Integer val = entry.getValue();
						if(!this.get(key).equals(val)){
							return false;
						}
					}
					
					for(Map.Entry<Character, Integer> entry : this.entrySet()){
						Character key = entry.getKey();
						Integer val = entry.getValue();
						if(!another.get(key).equals(val)){
							return false;
						}
					}
					
				}
				return true;
			}
		};
		map.put('a', 1);
		map.put('b', 1);
		map.put('c', 1);
		map.put('d', 2);
		map.put('e', 1);
		assertTrue(map.equals(stones.getGemStoneFrequency("abcdde")));
		
		map.clear();
		map.put('a', 1);
		map.put('b', 2);	
		map.put('e', 1);
		assertTrue(map.equals(stones.getGemStoneFrequency("abbe")));
	}

}
