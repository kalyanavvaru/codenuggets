package codenuggets.hackerrank;
import java.util.ArrayList;
import java.util.Scanner;


public class UtopianTreeSolution {

    public static void main(String[] args) {
    	UtopianTreeSolution runner = new UtopianTreeSolution();
    	for(int a : runner.doJob()){
    		System.out.println(a);	
    	}
    	
    }

	public Integer[] doJob() {
		ArrayList<Integer> result = new ArrayList<Integer>();
		Scanner in  = new Scanner(System.in);		
    	int numberOfTestCases = in.nextInt();
    	for(int i = 0; i < numberOfTestCases ; i++){
    		int numberOfCycles = in.nextInt();
    		result.add(lengthAfterCycles(numberOfCycles));
    	}
    	return result.toArray(new Integer [result.size()] );
	}

	public int lengthAfterCycles(int cycles){
		int fullCycles = cycles / 2;
		int halfCycles = cycles % 2;
		int totalLength = 1;
		for(int i = 0 ; i < fullCycles ; i++){
			totalLength = doFullCycle(totalLength);
		}
		
		for(int i = 0 ; i < halfCycles ; i++){
			totalLength = doHalfCycle(totalLength);
		}
		
		return totalLength;
	}
	
	public int doDouble(int x){
		return x > 0 ? x * 2 : 0;
	}

	public Object doPlusOne(int i) {
		return i > 0 ? i + 1 : 0;
	}
	
	public int doHalfCycle(int x){
		return doDouble(x);
	}
	
	public int doFullCycle(int x){
		return x > 0 ? doDouble(x) + 1 : 0;
	}
}
