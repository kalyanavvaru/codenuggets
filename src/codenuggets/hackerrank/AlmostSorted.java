package codenuggets.hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class AlmostSorted {

	public static void main(String[] args) throws IOException {
		AlmostSorted as = new AlmostSorted();

		long[] input = as.readArrayFromKeyBoard();
		System.out.println(as.countAlmostSorted(input));
	}
//	{3, 1, 2, 5, 4}
//	 0  1  2  3  4
	public long countAlmostSorted(long[] arr) {
		long[] copyArr = Arrays.copyOf(arr, arr.length);
		Arrays.sort(copyArr);
		
		long maxValue = copyArr[arr.length-1];
		
		long maxIndex = -1;
		
		for (int i = 0; i < copyArr.length; i++) {
			if(maxValue == copyArr[i]){
				maxIndex = i;
				break;
			}
			
		}
		
//		System.out.println("maxIndex : "+maxIndex);
		
		long count = 0;
		for (int i = 0; i <=maxIndex ; i++) {
			for (int j = i+1; j <=maxIndex; j++) {
 				if(arr[i] < arr[j]){
 					long[] rangeArray = Arrays.copyOfRange(arr, i, j+1);
// 					System.out.println("(i, j) = "+i+","+j+":"+Arrays.toString(rangeArray));
 					Arrays.sort(rangeArray);
 					int maxVal = (int) rangeArray[rangeArray.length-1];
 					int minVal = (int) rangeArray[0];
// 					System.out.println("after (i, j) = "+i+","+j+":"+Arrays.toString(rangeArray));
// 					System.out.println("equals :"+Arrays.equals(rangeArray, Arrays.copyOfRange(arr, i, j+1)));
 					if(rangeArray.length>0 && maxVal == arr[j] && minVal == arr[i]){
 						count+=1;
// 						System.out.println("	-	-	- "+i+"-"+j+" >< "+arr[i]+" "+arr[j]+" count="+count);
 					}
				}else{
//					System.out.println("skipping series starting.."+arr[i]+"");
					break;
				}
			}
		}
		return count+arr.length;
	}

	

	private long[] readArrayFromKeyBoard() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		long length = Long.parseLong(br.readLine());
		long[] input = new long[(int) length];
		String line = br.readLine();
		String[] array = line.split(" ");
		for (int i = 0; i < length; i++) {
			input[i] = Long.parseLong(array[i]);
		}
		return input;
	}

}
