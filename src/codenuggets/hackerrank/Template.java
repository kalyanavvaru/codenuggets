package codenuggets.hackerrank;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Template {

	public static void main(String[] args) throws NumberFormatException, IOException {
		Template template = new Template();
		long input = template.readInputFromKeyBoard();
		System.out.println(template.generateBase7(input));
	}

	public String generateBase7(long number) {
		String returnVal = "";
		char[] base7 = new char[]{'0','a','t','l','s','i','n'};
		do{
			returnVal = base7[(int) (number%7)]+returnVal;
			number=number/7;
		}while(number>=7);
			
		returnVal = base7[(int)number]+returnVal; 
				
		return returnVal;
		
	}

	public long readInputFromKeyBoard() throws IOException {
		List<String> returnString = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String numString = "";
		long numOfTestCases = 0;
		try {
			numString = br.readLine();
			numOfTestCases = Long.parseLong(numString);
		} catch (NumberFormatException e) {
			System.out.println("Not a number "+numString);
			System.exit(1);
		}
		return numOfTestCases;
	}
	
	

}
