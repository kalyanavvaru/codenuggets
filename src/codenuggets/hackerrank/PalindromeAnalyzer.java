package codenuggets.hackerrank;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PalindromeAnalyzer {

	public static void main(String[] args) {
		PalindromeAnalyzer analyzer = new PalindromeAnalyzer();
		
		List<String> inputStrings = new ArrayList<String>();
		
		Scanner in = new Scanner(System.in,"UTF-8");
		
		int numberofTests = in.nextInt();
		
		for(int i = 0 ;i < numberofTests ; i++){
			 inputStrings.add(in.next());
		}
		
		for(int i :analyzer.getNumberofEditsRequired(inputStrings)){
			System.out.println(i);
		}
	}

	public List<Integer> getNumberofEditsRequired(List<String> inputStrings) {
		List<Integer> output = new ArrayList<Integer>();
		for(String input : inputStrings){
			int numberOfEdits = getNumberofEditsRequired(input);
			output.add(numberOfEdits);
		}
		return output;
	}

	public int getNumberofEditsRequired(String input) {
		List<ReflectionPair> pairs = analyzePalindrome(input);
		int numberOfEdits = 0;
		for(ReflectionPair pair : pairs){
			int deletions = Math.abs(input.charAt(pair.getX()) - input.charAt(pair.getY()));
			numberOfEdits+=deletions;
		}
		return numberOfEdits;
	}
	
	public List<ReflectionPair> analyzePalindrome(String string) {
		List<ReflectionPair> pair = new ArrayList<ReflectionPair>();
		int i = 0;
		int j = string.length()-1;
		outer: for (; i < string.length()/2; i++) {
			for (; j >= 0;) {
				if (string.charAt(i) != string.charAt(j)) {
					pair.add(new ReflectionPair(i, j));
				}
				j--;
				continue outer;
			}
		}

		return pair;
	}

	static class ReflectionPair implements Comparable<ReflectionPair>{
		int x;
		int y;

		public int getX() {
			return this.x;
		}

		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return this.y;
		}

		public void setY(int y) {
			this.y = y;
		}

		public ReflectionPair(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public int hashCode() {
			return x ^ y;
		}

		@Override
		public boolean equals(Object obj) {
			boolean returnVal = false;
			if (obj instanceof ReflectionPair) {
				ReflectionPair pair = (ReflectionPair) obj;
				if (pair.x == this.x && pair.y == this.y) {
					returnVal = true;
				}
			}
			return returnVal;
		}

		@Override
		public String toString() {
			return x + ":" + y;
		}

		@Override
		public int compareTo(ReflectionPair o) {
			return Integer.valueOf(o.x).compareTo(this.x);
		}
	}

}
