import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;




public class StringBasedRSAKeys {
	public static void main(String[] args){
		try {
			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
			keyPairGenerator.initialize(2048);
			KeyPair keyPair = keyPairGenerator.generateKeyPair();
			PublicKey publicKey = keyPair.getPublic();
			PrivateKey privateKey = keyPair.getPrivate();
			
			byte[] publicKeyBytes = publicKey.getEncoded();
			byte[] publicKeyBase64 = Base64.encodeBase64(publicKeyBytes);
			byte[] privateKeyBytes = privateKey.getEncoded();
			byte[] privateKeyBase64 = Base64.encodeBase64(privateKeyBytes);
			
			System.out.println("----------Below can be saved and distributed between parties------------------");
			System.out.println("Base64 PUBLIC KEY : "+new String(publicKeyBase64));
			System.out.println("Base64 PRIVATE KEY: "+new String(privateKeyBase64));
			System.out.println("------------------------------------------------------------------------------");
			
			String message = "username=admin@unitingcare\ntimestamp=2014-02-01 22:49:00 +9:30";
			
			byte[] encryptedMessage = encyrpt(message,publicKeyBase64);
			System.out.println("encrypted message : "+new String(encryptedMessage));
			
			byte[] decyptedMessage = decrypt(encryptedMessage,privateKeyBase64);
			System.out.println("decrypted message : "+new String(decyptedMessage));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static byte[] encyrpt(String string, byte[] publicKeyBase64) {
		try {
			byte[] publicKeyBytes = Base64.decodeBase64(publicKeyBase64);
			Cipher rsa = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			
			KeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PublicKey publicKey = keyFactory.generatePublic(keySpec);
			
			rsa.init(Cipher.ENCRYPT_MODE, publicKey);
			byte[] ciphertext = rsa.doFinal(string.getBytes());
			ciphertext = Base64.encodeBase64(ciphertext);
			return ciphertext;			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}

	private static byte[] decrypt(byte[] encryptedMessage, byte[] privateKeyBase64) {
		try {
			byte[] privateKeyBytes = Base64.decodeBase64(privateKeyBase64);
			Cipher rsa = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			
			KeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
			
			rsa.init(Cipher.DECRYPT_MODE, privateKey);
			encryptedMessage = Base64.decodeBase64(encryptedMessage);
			byte[] decrypted = rsa.doFinal(encryptedMessage);
			return decrypted;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
