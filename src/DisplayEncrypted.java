import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

public class DisplayEncrypted {
	public static void main(String[] args) {
		
		String publicKeyBase64 =  "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxM5rDMRmyTVbPRgkOUcT2XHLjm1vPDDdK4pYDFBsCSqGxttJAx3rH34bkdobKctRja3ugpP4JYEXZzPliocAGwdsAq82PBy/KsPgA7tbq0GA4NcSv+KlVXqnbgOj95/NP8PRhu/Usxz7B8IsBMFQkXvnmcaw1uzfZPQvILDKiG+9q7B8WMlUPYhdOyvhCu4n6UkXrzHOIweM8tiiXBNiYXvx2+30+B69tdoqubIsFioGlyz00LJfD2a834d61FpvC2nODtjdC1KoNHaybL9ML+t+lbSFd7hfIRNGfF2iGwjj429mVSZQl1ZQWJDMlhhFckI0eLN7n1M6XSfi1vBdGwIDAQAB";
		String privateKeyBase64 = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDEzmsMxGbJNVs9GCQ5RxPZccuObW88MN0rilgMUGwJKobG20kDHesffhuR2hspy1GNre6Ck/glgRdnM+WKhwAbB2wCrzY8HL8qw+ADu1urQYDg1xK/4qVVeqduA6P3n80/w9GG79SzHPsHwiwEwVCRe+eZxrDW7N9k9C8gsMqIb72rsHxYyVQ9iF07K+EK7ifpSRevMc4jB4zy2KJcE2Jhe/Hb7fT4Hr212iq5siwWKgaXLPTQsl8PZrzfh3rUWm8Lac4O2N0LUqg0drJsv0wv636VtIV3uF8hE0Z8XaIbCOPjb2ZVJlCXVlBYkMyWGEVyQjR4s3ufUzpdJ+LW8F0bAgMBAAECggEBAL4LKfpRKYRAntM3HbultDg0l8D4UgLImLozxGRXwIt1YmJiXHXDDUoC57KG79YC/MSYYYn8QxJMPwIsbgq/5E01dIp/ERspEzzDmu1fndyYy0/Sv8tFnCwX3dfxcgNrb7KUcB8iNr2jTZEnYoRQ/HEQOgwXfqp8YBhHZ77kmR73Pnb2i540XIcqxeQDsyetL2qV+Qf5dQdtoHCKLQlSylOmDBqYGZHQ2VCZQRK+n6/6nonQYMNfGedB7Nb/cduhrGa1QVtVnbgTkol3JcWweI5jEilyRgT23y/jUBHaRTZtsxJkB5jgsa+xz4XtzprjoHkAAktGX4knN6DkEvSimhECgYEA53bjC/ddIEOp31XzNhAk6keO4qARmhS+4jpvbk8bs7PjV2TbN09m/joLwsbUnToHZWUEbQ32zAskJseMI8Ta9OcpFftIFtgLsH/nM5EsjEeM/lRzKOwLF4FcsdXx0BAmvF+NwkNNfV33MJoeNELhed4IrFm3Ub/TbJj92i+fpOMCgYEA2asJC+1j9LoyUe/pEN9ZqGS1RxjIoxWr+Qv3SMnQYGn7tisF0/PTw7NxCUdwhKSy3v0QxUPJewTkUA0uE1HGup/d//oyv1+OAby7rBA6gLXRYNFPmk13X3ikiQT9AZdGtjaHYWzwgeXih/xNJHjTRypJi6KZyg9/FrJUhFxcFGkCgYBFu2trj3pe3gOvZRWAlByK+EPzwoWLBfT78t7fPLX7zZYQHjJs118P+CzS4VPsii7wPR64PBy+HB0oEbYGkKfmYD3ggXOWbkAXTHE69+GT4Xp4pLiM+4/b5P0oFmxjYnpJBgCPJfeSbVyrJ8Mxu3RyYpH6Woz/8+qeLPlNYkuWQQKBgQDUPpJzrubomya1/tmkOiWSraCV0vTRETC/h4t4zlEBslZfK1RjeD3ldfOiuKZesLo59UmFELRF4F9AL5nd/L9q+rfeAnm5YdN2njHhdXGWT7P6VB7qtt7PmTPW56ZNBtVMl58qwK9D+oUJVFtkVOyvV+ye285Z2QJzfzAqYGTpAQKBgEi+MghWCg/HChz8IJ6sEOlI+Th8Pxz7ooVnuGTIS8HHKmXcGGYDJxCGO+NDkyWZxYZ0kaZbIv3pUifNzANZELgwxZD/kBnrqMcC/sCrCAM5JrkvjciWQC7selSGA5B4KGRh1ZVpyIAxr/2hXV9cUKlaSGdsOL6pgmeQ6SO4QgGW";
		
		String message = "username=16212&timestamp=2014-02-03 14:49:00 +9:30";
		
		byte[] encryptedMessage = encyrpt(message,publicKeyBase64.getBytes());
		System.out.println("encrypted message : "+new String(encryptedMessage));
		byte[] decryptedMessage = decrypt(encryptedMessage, privateKeyBase64.getBytes());
		System.out.println("decrypted message : "+new String(decryptedMessage));
		
	}
	private static byte[] encyrpt(String string, byte[] publicKeyBase64) {
		try {
			byte[] publicKeyBytes = Base64.decodeBase64(publicKeyBase64);
			Cipher rsa = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			
			KeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PublicKey publicKey = keyFactory.generatePublic(keySpec);
			
			rsa.init(Cipher.ENCRYPT_MODE, publicKey);
			byte[] ciphertext = rsa.doFinal(string.getBytes());
			ciphertext = Base64.encodeBase64(ciphertext);
			return ciphertext;			
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	private static byte[] decrypt(byte[] encryptedMessage, byte[] privateKeyBase64) {
		try {
			byte[] privateKeyBytes = Base64.decodeBase64(privateKeyBase64);
			Cipher rsa = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			
			KeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
			
			rsa.init(Cipher.DECRYPT_MODE, privateKey);
			encryptedMessage = Base64.decodeBase64(encryptedMessage);
			byte[] decrypted = rsa.doFinal(encryptedMessage);
			return decrypted;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
